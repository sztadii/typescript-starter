import React, { ReactText } from 'react'
import { Wrapper, InputElement, Label } from './Input.style'

export interface InputChangeObj {
  name: string
  value: ReactText
}

interface InputProps {
  label: string
  name: string
  value: ReactText
  onChange: (e: InputChangeObj) => void
}

export default function Input(props: InputProps) {
  const { label, name, value, onChange } = props
  return (
    <Wrapper>
      <Label>{label}</Label>
      <InputElement
        value={value}
        name={name}
        onChange={e => onChange({ name, value: Number(e.target.value) || e.target.value })}
      />
    </Wrapper>
  )
}
