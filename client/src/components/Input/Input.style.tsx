import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
`

export const InputElement = styled.input`
  background: #f0f0f0;
  display: block;
  width: 100%;
  border: none;
  margin-bottom: 10px;
  padding: 10px;
  border-radius: 4px;
`

export const Label = styled.div`
  padding: 10px;
  width: 200px;
`
