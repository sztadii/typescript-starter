import { cleanup, render, wait } from '@testing-library/react'
import React from 'react'
import { MockedProvider } from 'react-apollo/test-utils'
import { GetProductsDocument } from '../../graphql/clientTypes'
import App from './App'

const mocks = [
  {
    request: {
      query: GetProductsDocument
    },
    result: {
      data: {
        users: [
          {
            email: 'sztadii@gmail.com'
          }
        ]
      }
    }
  }
]

const errorMocks = [
  {
    error: new Error('Something went wrong'),
    request: {
      query: GetProductsDocument
    }
  }
]

function renderComponent(mock: any = mocks) {
  return render(
    <MockedProvider mocks={mock} addTypename={false}>
      <App />
    </MockedProvider>
  )
}

beforeEach(cleanup)

test('Options render loader during fetching data', () => {
  const { getByText } = renderComponent()

  getByText('Loading...')
})

test('Options render error from server', async () => {
  const { getByText } = renderComponent(errorMocks)

  await wait(() => {
    getByText('Error :(')
  })
})
