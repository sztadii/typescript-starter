import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Container from '../Container/Container'
import NavBar from '../NavBar/NavBar'
import Home from '../Home/Home'
import Products from '../Products/Products'
import { routerPaths } from '../../config/defaults'
import { GlobalStyle } from './App.style'

export default function App() {
  return (
    <div>
      <Router>
        <NavBar />
        <Container>
          <Route exact path={routerPaths.home} component={Home} />
          <Route path={routerPaths.products} component={Products} />
        </Container>
      </Router>
      <GlobalStyle />
    </div>
  )
}
