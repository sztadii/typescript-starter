import React from 'react'
import ProductCreate from '../ProductCreate/ProductCreate'
import ProductList from '../ProductList/ProductList'

export default function Products() {
  return (
    <div>
      <ProductCreate />
      <ProductList />
    </div>
  )
}
