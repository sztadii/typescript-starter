import React from 'react'
import { GetProductsDocument, RemoveAllProductsComponent } from '../../graphql/clientTypes'
import Button from '../Button/Button'
import { Wrapper } from './Options.style'

export default function Options() {
  return (
    <Wrapper>
      <RemoveAllProductsComponent refetchQueries={[{ query: GetProductsDocument }]}>
        {mutate => <Button onClick={() => mutate()}>Remove all products</Button>}
      </RemoveAllProductsComponent>
    </Wrapper>
  )
}
