import styled from 'styled-components'

export default styled.button`
  background: #4e71ff;
  color: white;
  text-transform: uppercase;
  padding: 10px 20px;
  border-radius: 4px;
  border: none;
`
