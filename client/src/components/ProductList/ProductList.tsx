import React from 'react'
import { GetProductsComponent } from '../../graphql/clientTypes'
import Options from '../Options/Options'
import { Item } from './ProductList.style'

export default function ProductList() {
  return (
    <GetProductsComponent>
      {({ data, error, loading }) => {
        if (loading || !data) {
          return <div>Loading...</div>
        }

        if (error) {
          return <div>Error :(</div>
        }

        const { products } = data

        return (
          <>
            {!!products.length && <Options />}
            {products.length ? (
              products.map(product => <Item key={product.id}>{product.name}</Item>)
            ) : (
              <Item>Empty list</Item>
            )}
          </>
        )
      }}
    </GetProductsComponent>
  )
}
