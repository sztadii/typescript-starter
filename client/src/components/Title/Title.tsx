import styled from 'styled-components'

export default styled.h1`
  display: block;
  background: #f0f0f0;
  text-transform: uppercase;
  font-size: 18px;
  text-align: center;
  padding: 20px 0;
  border-radius: 4px;
  margin-bottom: 20px;
`
