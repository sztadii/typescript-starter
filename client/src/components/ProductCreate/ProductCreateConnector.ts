import { compose } from 'react-apollo'
import { withCreateProduct } from '../../graphql/clientTypes'

export default compose(withCreateProduct())
