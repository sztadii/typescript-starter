import React, { Component } from 'react'
import startCase from 'lodash/startCase'
import { GetProductsDocument, CreateProductProps, ProductInput } from '../../graphql/clientTypes'
import Button from '../Button/Button'
import Input, { InputChangeObj } from '../Input/Input'
import Title from '../Title/Title'
import { Box } from './ProductCreate.style'
import connector from './ProductCreateConnector'

interface FormState {
  error: string
  input: ProductInput
}

class ProductCreate extends Component<CreateProductProps, FormState> {
  state: FormState = {
    error: '',
    input: {
      name: '',
      type: 'food',
      calories: 0,
      carbohydrates: 0,
      fibres: 0,
      fats: 0,
      proteins: 0,
      servingQuantity: 100,
      unitSymbol: 'g'
    }
  }

  handleChange = (e: InputChangeObj) => {
    this.setState(state => ({
      error: '',
      input: {
        ...state.input,
        [e.name]: e.value
      }
    }))
  }

  handleSubmit = async () => {
    const { input } = this.state
    const { mutate } = this.props

    if (!mutate) {
      return
    }

    try {
      await mutate({
        variables: {
          input
        },
        refetchQueries: [{ query: GetProductsDocument }],
        awaitRefetchQueries: true
      })
    } catch (e) {
      this.setState({
        error: e.toString()
      })
    }
  }

  render() {
    const { input, error } = this.state
    const keys = Object.entries(input)

    return (
      <div>
        <Title>Create product</Title>
        <Box>
          {keys.map(([key, value]) => (
            <Input
              key={key}
              name={key}
              value={value}
              label={startCase(key)}
              onChange={this.handleChange}
            />
          ))}
        </Box>
        <Box>
          <Button onClick={this.handleSubmit}>Create product</Button>
        </Box>
        <Box>{error && <div>{error}</div>}</Box>
      </div>
    )
  }
}

export default connector(ProductCreate)
