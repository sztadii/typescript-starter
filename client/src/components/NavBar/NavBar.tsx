import React from 'react'
import { NavLink } from 'react-router-dom'
import { routerPaths } from '../../config/defaults'
import { Wrapper, Item } from './NavBar.style'

export default function NavBar() {
  return (
    <Wrapper>
      <NavLink to={routerPaths.home}>
        <Item>Home</Item>
      </NavLink>
      <NavLink to={routerPaths.products}>
        <Item>Create product</Item>
      </NavLink>
    </Wrapper>
  )
}
