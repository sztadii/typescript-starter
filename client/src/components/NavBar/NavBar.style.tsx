import styled from 'styled-components'

export const Wrapper = styled.div`
  margin-bottom: 20px;
  display: flex;
  background: #4e71ff;
  color: white;
  text-transform: uppercase;
`

export const Item = styled.div`
  padding: 10px 20px;
`
