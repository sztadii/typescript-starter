import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'

const client = new ApolloClient({
  cache: new InMemoryCache({
    addTypename: false
  }),
  link: createHttpLink({ uri: 'http://localhost:4000/graphql' })
})

export default client
