/* tslint:disable */
import gql from 'graphql-tag'
import * as ReactApollo from 'react-apollo'
import * as React from 'react'
export type Maybe<T> = T | null
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
}

export type Mutation = {
  createProduct: Product
  removeAllProducts: Scalars['Boolean']
}

export type MutationCreateProductArgs = {
  input: ProductInput
}

export type Product = {
  id: Scalars['ID']
  type: Scalars['String']
  name: Scalars['String']
  unitSymbol: Scalars['String']
  servingQuantity: Scalars['Int']
  calories: Scalars['Int']
  proteins: Scalars['Int']
  carbohydrates: Scalars['Int']
  fats: Scalars['Int']
  fibres: Scalars['Int']
}

export type ProductInput = {
  type: Scalars['String']
  name: Scalars['String']
  unitSymbol: Scalars['String']
  servingQuantity: Scalars['Int']
  calories: Scalars['Int']
  proteins: Scalars['Int']
  carbohydrates: Scalars['Int']
  fats: Scalars['Int']
  fibres: Scalars['Int']
}

export type Query = {
  products: Array<Product>
}
export type CreateProductMutationVariables = {
  input: ProductInput
}

export type CreateProductMutation = { createProduct: Pick<Product, 'name'> }

export type GetProductsQueryVariables = {}

export type GetProductsQuery = { products: Array<Pick<Product, 'id' | 'name'>> }

export type RemoveAllProductsMutationVariables = {}

export type RemoveAllProductsMutation = Pick<Mutation, 'removeAllProducts'>

export const CreateProductDocument = gql`
  mutation CreateProduct($input: ProductInput!) {
    createProduct(input: $input) {
      name
    }
  }
`
export type CreateProductMutationFn = ReactApollo.MutationFn<
  CreateProductMutation,
  CreateProductMutationVariables
>
export type CreateProductComponentProps = Omit<
  ReactApollo.MutationProps<CreateProductMutation, CreateProductMutationVariables>,
  'mutation'
>

export const CreateProductComponent = (props: CreateProductComponentProps) => (
  <ReactApollo.Mutation<CreateProductMutation, CreateProductMutationVariables>
    mutation={CreateProductDocument}
    {...props}
  />
)

export type CreateProductProps<TChildProps = {}> = Partial<
  ReactApollo.MutateProps<CreateProductMutation, CreateProductMutationVariables>
> &
  TChildProps
export function withCreateProduct<TProps, TChildProps = {}>(
  operationOptions?: ReactApollo.OperationOption<
    TProps,
    CreateProductMutation,
    CreateProductMutationVariables,
    CreateProductProps<TChildProps>
  >
) {
  return ReactApollo.withMutation<
    TProps,
    CreateProductMutation,
    CreateProductMutationVariables,
    CreateProductProps<TChildProps>
  >(CreateProductDocument, {
    alias: 'withCreateProduct',
    ...operationOptions
  })
}
export const GetProductsDocument = gql`
  query GetProducts {
    products {
      id
      name
    }
  }
`
export type GetProductsComponentProps = Omit<
  ReactApollo.QueryProps<GetProductsQuery, GetProductsQueryVariables>,
  'query'
>

export const GetProductsComponent = (props: GetProductsComponentProps) => (
  <ReactApollo.Query<GetProductsQuery, GetProductsQueryVariables>
    query={GetProductsDocument}
    {...props}
  />
)

export type GetProductsProps<TChildProps = {}> = Partial<
  ReactApollo.DataProps<GetProductsQuery, GetProductsQueryVariables>
> &
  TChildProps
export function withGetProducts<TProps, TChildProps = {}>(
  operationOptions?: ReactApollo.OperationOption<
    TProps,
    GetProductsQuery,
    GetProductsQueryVariables,
    GetProductsProps<TChildProps>
  >
) {
  return ReactApollo.withQuery<
    TProps,
    GetProductsQuery,
    GetProductsQueryVariables,
    GetProductsProps<TChildProps>
  >(GetProductsDocument, {
    alias: 'withGetProducts',
    ...operationOptions
  })
}
export const RemoveAllProductsDocument = gql`
  mutation RemoveAllProducts {
    removeAllProducts
  }
`
export type RemoveAllProductsMutationFn = ReactApollo.MutationFn<
  RemoveAllProductsMutation,
  RemoveAllProductsMutationVariables
>
export type RemoveAllProductsComponentProps = Omit<
  ReactApollo.MutationProps<RemoveAllProductsMutation, RemoveAllProductsMutationVariables>,
  'mutation'
>

export const RemoveAllProductsComponent = (props: RemoveAllProductsComponentProps) => (
  <ReactApollo.Mutation<RemoveAllProductsMutation, RemoveAllProductsMutationVariables>
    mutation={RemoveAllProductsDocument}
    {...props}
  />
)

export type RemoveAllProductsProps<TChildProps = {}> = Partial<
  ReactApollo.MutateProps<RemoveAllProductsMutation, RemoveAllProductsMutationVariables>
> &
  TChildProps
export function withRemoveAllProducts<TProps, TChildProps = {}>(
  operationOptions?: ReactApollo.OperationOption<
    TProps,
    RemoveAllProductsMutation,
    RemoveAllProductsMutationVariables,
    RemoveAllProductsProps<TChildProps>
  >
) {
  return ReactApollo.withMutation<
    TProps,
    RemoveAllProductsMutation,
    RemoveAllProductsMutationVariables,
    RemoveAllProductsProps<TChildProps>
  >(RemoveAllProductsDocument, {
    alias: 'withRemoveAllProducts',
    ...operationOptions
  })
}
