# typescript-starter

> Typescript starter for full-stack graphql, react and type-orm apps

### Setup

- Install docker
- Install only required dependencies `npm ci`
- Run db `npm run db`
- Run client and server `npm start`
- Open client dashboard at `http://localhost:3000`
- Open server dashboard at `http://localhost:4000/graphql`

### Check your server / client types

- Generate typescript types from schema.graphql and validate it on client and server side `npm run types`
