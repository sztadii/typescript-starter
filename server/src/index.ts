import { ApolloServer, gql } from 'apollo-server-express'
import * as express from 'express'
import * as fs from 'fs'
import { createConnection } from 'typeorm'
import { resolvers } from './resolvers'

async function startServer() {
  const typeDefs = gql(fs.readFileSync(__dirname.concat('/schema.graphql'), 'utf8'))
  // @ts-ignore
  const server = new ApolloServer({ typeDefs, resolvers })

  await createConnection()

  const app = express()

  server.applyMiddleware({ app })

  app.listen({ port: 4000 }, () => console.log(`🚀  Server ready at ${server.graphqlPath}`))
}

startServer()
