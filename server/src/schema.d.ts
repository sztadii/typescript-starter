/* tslint:disable */
import { GraphQLResolveInfo } from 'graphql'
export type Maybe<T> = T | null
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
}

export type Mutation = {
  createProduct: Product
  removeAllProducts: Scalars['Boolean']
}

export type MutationCreateProductArgs = {
  input: ProductInput
}

export type Product = {
  id: Scalars['ID']
  type: Scalars['String']
  name: Scalars['String']
  unitSymbol: Scalars['String']
  servingQuantity: Scalars['Int']
  calories: Scalars['Int']
  proteins: Scalars['Int']
  carbohydrates: Scalars['Int']
  fats: Scalars['Int']
  fibres: Scalars['Int']
}

export type ProductInput = {
  type: Scalars['String']
  name: Scalars['String']
  unitSymbol: Scalars['String']
  servingQuantity: Scalars['Int']
  calories: Scalars['Int']
  proteins: Scalars['Int']
  carbohydrates: Scalars['Int']
  fats: Scalars['Int']
  fibres: Scalars['Int']
}

export type Query = {
  products: Array<Product>
}

export type ResolverTypeWrapper<T> = Promise<T> | T

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult

export type StitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>
}

export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs>
  resolve?: SubscriptionResolveFn<TResult, TParent, TContext, TArgs>
}

export type SubscriptionResolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionResolverObject<TResult, TParent, TContext, TArgs>)
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes>

export type NextResolverFn<T> = () => Promise<T>

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Query: ResolverTypeWrapper<{}>
  Product: ResolverTypeWrapper<Product>
  ID: ResolverTypeWrapper<Scalars['ID']>
  String: ResolverTypeWrapper<Scalars['String']>
  Int: ResolverTypeWrapper<Scalars['Int']>
  Mutation: ResolverTypeWrapper<{}>
  ProductInput: ProductInput
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>
}

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Query: {}
  Product: Product
  ID: Scalars['ID']
  String: Scalars['String']
  Int: Scalars['Int']
  Mutation: {}
  ProductInput: ProductInput
  Boolean: Scalars['Boolean']
}

export type MutationResolvers<ContextType = any, ParentType = ResolversParentTypes['Mutation']> = {
  createProduct?: Resolver<
    ResolversTypes['Product'],
    ParentType,
    ContextType,
    MutationCreateProductArgs
  >
  removeAllProducts?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>
}

export type ProductResolvers<ContextType = any, ParentType = ResolversParentTypes['Product']> = {
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>
  type?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  unitSymbol?: Resolver<ResolversTypes['String'], ParentType, ContextType>
  servingQuantity?: Resolver<ResolversTypes['Int'], ParentType, ContextType>
  calories?: Resolver<ResolversTypes['Int'], ParentType, ContextType>
  proteins?: Resolver<ResolversTypes['Int'], ParentType, ContextType>
  carbohydrates?: Resolver<ResolversTypes['Int'], ParentType, ContextType>
  fats?: Resolver<ResolversTypes['Int'], ParentType, ContextType>
  fibres?: Resolver<ResolversTypes['Int'], ParentType, ContextType>
}

export type QueryResolvers<ContextType = any, ParentType = ResolversParentTypes['Query']> = {
  products?: Resolver<Array<ResolversTypes['Product']>, ParentType, ContextType>
}

export type Resolvers<ContextType = any> = {
  Mutation?: MutationResolvers<ContextType>
  Product?: ProductResolvers<ContextType>
  Query?: QueryResolvers<ContextType>
}

/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>
