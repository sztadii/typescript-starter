import { ProductEntity } from '../entity/ProductEntity'
import { Resolvers } from '../schema'

export const ProductResolver: Resolvers = {
  Mutation: {
    createProduct: (_, { input }) => ProductEntity.create(input).save(),
    removeAllProducts: () => !!ProductEntity.clear()
  },
  Query: {
    products: () => ProductEntity.find()
  }
}
