import { merge } from 'lodash'
import { ProductResolver } from './resolvers/ProductResolver'

export const resolvers = merge({}, ProductResolver)
