import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

// TODO Improve enums relations between graphql schema and ProductEntity

@Entity('products')
export class ProductEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  public id: string

  @Column({
    enum: ['food', 'drink'],
    type: 'enum'
  })
  public type: string

  @Column({
    unique: true
  })
  public name: string

  @Column({
    enum: ['g', 'ml'],
    type: 'enum'
  })
  public unitSymbol: string

  @Column()
  public servingQuantity: number

  @Column()
  public calories: number

  @Column()
  public proteins: number

  @Column()
  public carbohydrates: number

  @Column()
  public fats: number

  @Column()
  public fibres: number
}
