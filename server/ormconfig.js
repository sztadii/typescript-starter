const path = process.env.NODE_ENV === 'production' ? 'build' : 'src'

module.exports = {
  name: 'default',
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'docker',
  database: 'postgres',
  synchronize: true,
  logging: false,
  entities: [`${path}/entity/**/*.{js,ts}`],
  migrations: [`${path}/migration/**/*.{js,ts}`],
  subscribers: [`${path}/subscriber/**/*.{js,ts}`],
  cli: {
    entitiesDir: `${path}/entity`,
    migrationsDir: `${path}/migration`,
    subscribersDir: `${path}/subscriber`
  }
}
